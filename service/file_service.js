const models = require("../model/connectdb");
const Account = models.account;

const registerAccount = async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;
    try {
        const register = await Account.create({
        username: username,
        password: password,
        });
        console.log(register);
        res.status(201).json(register);
    } catch (error) {
        console.log(error);
        res.status(400).json(error);
    }
};
const loginAccount = async (req , res) => {
    // const username =req.body.username
    // const password = req.body.password
    try{
        const login = await Account.findOne({
            where : {
                username : req.body.username,
                password : req.body.password
            }
        })
        res.status(200).json("login successful "+ login)
    }catch(error) {
        console.log(error);
        res.status(400).json(error)
    }
}
const searchUsername =  async (req , res) => {
    const username =req.body.username
    try {
        const checkUsername = await Account.findOne({
            where : {
                username : username
            }
        })
        res.status(302).json( "this account " + checkUsername)
    } catch (error) {
        console.log(error);
        res.status(204).json(error)
    }
}
const updateAccount = async (req , res) => {
    const getId = req.body.id 
    try { 
     await Account.update(req.body,{
        where : {
            id : getId
        }
     })
     res.send('update successful')
    }catch(error) {
    console.log(error);
    res.send('cannot search this id')
    }
}
module.exports = {
  registerAccount,
  loginAccount,
  searchUsername,
  updateAccount
};
