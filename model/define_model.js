const { Sequelize , DataTypes } = require('sequelize');
const insert = require('../utils/insertEmodel');
module.exports = (sequelize) => {
const Account = sequelize.define('account' , {
    username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
      },
})
return Account
}