const config = require('../config/config_file')
const { Sequelize } = require('sequelize');
const model = {} 

const {database , username , password , dialect} = config
const sequelize = new Sequelize (database , username , password ,{
  dialect ,
  logging : false
})
model.sequelize = sequelize
model.account = require('./define_model')(sequelize)
model.sequelize.sync()
const check  = async () => {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}
check()
module.exports = model
