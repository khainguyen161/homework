const express = require('express')
const router = express.Router()
const fileControllor = require('../service/file_service')
router.post('/register',fileControllor.registerAccount)
router.get("/login" , fileControllor.loginAccount)
router.get("/search",fileControllor.searchUsername)
router.put('/update',fileControllor.updateAccount)
module.exports = router